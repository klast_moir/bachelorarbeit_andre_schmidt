# Prototyp zur Bachelorarbeit von André Schmidt - Matr.-Nr. 854986

## Ein interaktives System zur Exploration der Möglichkeiten für ein Zusammenspiel von Sprachsteuerung und visuellem Feedback

### Installation
1. Ordner mit dem Code vom USB-Stick in ein beliebiges Verzeichnis auf den Rechner kopieren, auf dem er ausgeführt werden soll.

2. *Wenn noch nicht vorhanden* Auf dem Rechner Node.js installieren. Installation wie auf https://nodejs.org/en/ beschrieben durchführen.

3. Im Rechner per Terminal in den Ordner gehen, in dem der Code liegt. Im Terminal den Befehl **yarn** eingeben. Daraufhin werden die Node Modules mit den Dependencies installiert.

4. Im Terminal **yarn start** eingeben - das Startscript wird ausgeführt und nach einigen Sekunden öffnet sich die Anwendung automatisch im Browser auf dem **Port 3000**. Wenn nicht, ist sie per Eingabe in die Browserzeile unter http://localhost:3000/ erreichbar.

*Bei der Entwicklung verwendete Konfiguration - ggf. diese zum Starten der Anwendung nutzen!*

* Node Version 10.1.0
* Yarn Version 1.12.3
* Code Editor - Visual Studio Code
* Browser Google Chrome

### Anwendung im Browser
Der vorliegende Prototyp simuliert eine Webseite. Das bedeutet, dass nicht alle Unterseiten und Pfade bis ins Detail ausgearbeitet wurden. Um die Spracherkennung nutzen zu können, muss zunächst das Menü Hauptspeisen und darin das Gericht Kohlrouladen (letztes Gericht) ausgewählt werden. Ab der Seite Kohlrouladen steht die Sprachsteuerung zur Verfügung und kann getestet werden.
