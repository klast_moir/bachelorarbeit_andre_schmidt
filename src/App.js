import React from 'react';
import { Route, Switch } from "react-router-dom";
import Hauptspeisen from "./components/Hauptspeisen";
import Vorspeisen from "./components/Vorspeisen";
import Nachspeisen from "./components/Nachspeisen";
import Home from "./components/Home";
import KohlrouladenAnleitung from "./components/Kohlrouladen_anleitung";
import Kohlrouladen from "./components/Kohlrouladen";
import './App.css';
import styled, { keyframes } from 'styled-components';
import styles from './shared/global_styles';
import {
  useSpeechStateContext,
} from './shared/speechState_context';

const annyang = require('annyang');

const App = () => {
  const [isRecording, setIsRecording] = React.useState(false);
  const [speechPause, setSpeechPause] = React.useState(false);
  const [detection, setDetection] = React.useState([]);
  const speechRecAvailable = useSpeechStateContext();

  //Extract true or false for Availabilty SpeechRec from stateContext - a flag
  const isSpeechAvailable = Object.values(speechRecAvailable)[0];

  if(annyang) {
    annyang.setLanguage('de');
  }else{
    console.log("annyang nicht gefunden");
  }

  //Annyang Speech Commands
  const speechControl = () => {
    document.getElementById("speechControl").click();
  };

  const displayPause = () => {
    setSpeechPause(true);
  }

  const displayResume = () => {
    setSpeechPause(false);
  }

  const commands = {
    'Spracherkennung ausschalten': speechControl,
    'Anzeige aus': displayPause,
    'Anzeige an': displayResume,
  };

  annyang.addCommands(commands);
  annyang.debug();

//dynamic display of recognized speech
React.useEffect(() => {
  annyang.addCallback('result', function(phrases) {
    var phrasesAsString = phrases.join(' / ');
    if(detection.length !== phrases.length){
      setDetection(phrasesAsString);
    }
  }, [detection]);
})

//control to stop displaying recognized speech
const handleSpeechControl = () => {
  if (isRecording) {
    annyang.abort();
    setIsRecording(false);
    setDetection([]);          //set detection display to null
  } else {
    annyang.start();
    setIsRecording(true);
    }
  }

  const fadeInFlag = keyframes`
    0% {
      height: 0;
      background: white;
    }
    100% {
      height: 3vw;
      background: red;
    }
  `;

const SpeechRecFlag = styled.div`
  display: inline-block;
  width: 15vw;
  position: relative;

  animation-name: ${fadeInFlag};
  animation-duration: 1s;
  animation-timing-function: ease;
  animation-delay: 0s;
  animation-iteration-count: 1;
  animation-direction: normal;
  animation-fill-mode: forwards;
  animation-play-state: running;
`;

  const StyledFeedback = styled.div`
  background-color: white;
  grid-column: 3 / 8;
  grid-row: 1;
  font-size: ${styles.fontSizes.small};
  padding: .5vw;
  display: ${!isRecording ? 'none': 'block'};
  height: 1.75vw;
  border-bottom: .25vw solid red;
`;

const StyledVoiceControl = styled.div`
  width: 100%;
  background-color: black;
  display: grid;
  grid-template-columns: repeat(10, 1fr);
  position: fixed;
  top: 0;
  z-index: 20;
  height: 3vw;
`;

const StyledButton = styled.button`
  grid-column: 9 / 11;
  grid-row: 1;
  padding: 2%;
  margin: .5vw;
  font-size: ${styles.fontSizes.small};
  height: 2vw;
`;

const StyledImg = styled.img`
  width: ${styles.fontSizes.small};
  margin-right: ${styles.spacings.xsmall};
`;

return (
  <React.Fragment>
    <StyledVoiceControl>
      <StyledButton id='speechControl' onClick={handleSpeechControl}>
        {isRecording ? <StyledImg src='img/arrow_right.png' alt='Hilfe schließen' /> : null}
        {!isRecording ? "Spracherkennung anschalten" : "Spracherkennung ausschalten" }
      </StyledButton>
      <div>
        {isRecording ? <SpeechRecFlag /> : <div></div>}
      </div>
      <StyledFeedback>{speechPause ? ('Anzeige ist inaktiv => zum aktivieren sprich "Anzeige an"')
          : (
              !isSpeechAvailable ? 'Auf dieser Seite ist keine Sprachsteuerung verfügbar'
            : detection.length === 0 ? 'Keine Kommandos erkannt => Icons + Schrift = Sprachkommandos'
            : detection
          )
        }
      </StyledFeedback>
    </StyledVoiceControl>
    <Switch>
      <Route path="/vorspeisen" component={Vorspeisen} />
      <Route path="/hauptspeisen" component={Hauptspeisen} />
      <Route path="/nachspeisen" component={Nachspeisen} />
      <Route path="/kohlrouladen" component={Kohlrouladen} />
      <Route path="/kohlrouladen_anleitung" component={KohlrouladenAnleitung} />
      <Route path="/" component={Home} />
    </Switch>
  </React.Fragment>
  );
};

export default App;
