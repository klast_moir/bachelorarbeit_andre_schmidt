const styles = {
  fontSizes: {
    xxLarge: '10vw',
    xLarge: '7vw',
    large: '4vw',
    medium: '1.5vw',
    small: '1vw',
    banner: '7vw',
    h1: '5vw',
    h2: '3vw',
    h3: '2vw',
  },
  lineHeight: {
    large: '6vw',
    medium: '3vw',
    small: '2vw',
  },
  margins: {
    paragraph: '10vw',
    h1: '5vw',
    h2: '3vw',
    medium: '3vw',
  },
  spacings: {
    xLarge: '4vw',
    large: '3vw',
    medium: '2vw',
    small: '1vw',
    xsmall: '.5vw',
  },
  space: {
    iconSpace: '3vw',
    bannerHeight: '40vw',
  },
  colors: {
    backgroundHauptspeisen: '#fbffc9',
    backgroundHome: '#ffec80',
    backgroundInfo: '#eb3434',
    typoBlack: '#000000',
    typoWhite: '#ffffff',
  },
};

export default styles;
