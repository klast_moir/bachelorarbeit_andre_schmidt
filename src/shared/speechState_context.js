import React from 'react';

const SpeechStateContext = React.createContext();
const SpeechStateDispatch = React.createContext();

const initialState = {
    speechRecAvailable: false,
}

function reducer (state = {}, action) {
    switch (action.type) {
        case 'SET_SPEECH_AVAILABLE':
            return {
                ...state,
                speechRecAvailable: action.changeTo,
            }
        default: {
            throw new Error(`Nicht unterstützte Action ${action.type}`);
        }
    }
}

function SpeechRecProvider({children}){
    const[speechState, speechDispatch] = React.useReducer(reducer, initialState);

    return(
        <SpeechStateContext.Provider value={speechState}>
            <SpeechStateDispatch.Provider value={speechDispatch}>
                {children}
            </SpeechStateDispatch.Provider>
        </SpeechStateContext.Provider>
    );
}

const useSpeechStateContext = () => {
    const context = React.useContext(SpeechStateContext);
    if(context === undefined){
        throw new Error(`useSpeechStateContext muss im SpeechRecProvider sein`);
    }
    return context;
}

const useSpeechStateDispatch = () => {
    const dispatch = React.useContext(SpeechStateDispatch);
    if(dispatch === undefined){
        throw new Error(`useSpeechStateDispatch muss im SpeechRecProvider sein`);
    }
    return dispatch;
}

export { SpeechRecProvider, useSpeechStateContext, useSpeechStateDispatch };
