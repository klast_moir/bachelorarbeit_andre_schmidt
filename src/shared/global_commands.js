const GLOBAL_COMMANDS = ['Sprachsteuerung', 'Hilfe schließen', 'Spracherkennung ausschalten', '(scroll) hoch/runter', '(scroll) ganz hoch', 'Anzeige an/aus'];

export default GLOBAL_COMMANDS;
