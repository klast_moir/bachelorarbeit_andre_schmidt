import React from "react";
import styled from "@emotion/styled";
import styles from '../shared/global_styles';

const VerticalNav = () => {
  return(
    <FixedFlag>
      <p>(ganz) hoch <img src='img/arrow_up.png' alt='Hochscrollen' /></p>
      <p><img src='img/arrow_down.png' alt='Runterscrollen' />runter</p>
    </FixedFlag>
  )
}

const FixedFlag = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  position: fixed;
  z-index: 2;
  bottom: 0.5vw;
  left: 0;
  width: 3vw;
  background-color: ${styles.colors.backgroundHauptspeisen};
  padding: 0.25vw 0.5vw;

  p{
    text-align: center;
  }

  img{
    width: ${styles.fontSizes.medium};
    margin: 1vw 0;
  }
`;

export default VerticalNav;
