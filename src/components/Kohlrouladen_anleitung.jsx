import React from "react";
import { Link } from "react-router-dom";
import styled from "@emotion/styled";
import styles from '../shared/global_styles';
import SpeechHelp from './Kommando_Übersicht';
import GLOBAL_COMMANDS from '../shared/global_commands';
import { scrollDown, scrollUp, ganzHoch } from '../shared/global_voice_functions';
import VerticalNav from './VerticalNav';
import {
  useSpeechStateContext,
  useSpeechStateDispatch
} from '../shared/speechState_context';

const annyang = require('annyang');
const SPECIAL_COMMANDS = ['(zurück zu) Zutaten', '(Schritt) eins', '(Schritt) zwei', '(Schritt) drei', '(Schritt) vier', '(Schritt) fünf'];
const SPEECH_COMMANDS = SPECIAL_COMMANDS.concat(GLOBAL_COMMANDS);

const KohlrouladenAnleitung = () => {
  const dispatch = useSpeechStateDispatch();
  const speechRecAvailable = useSpeechStateContext();

  //signals that on this site no speechRec is available
  function setSpeechState(speechRecAvailable) {
    if(!speechRecAvailable){
        dispatch({type: 'SET_SPEECH_AVAILABLE', changeTo: true});
    }
  };

  React.useEffect(() => {
     let active = true;
       if(active){
           setSpeechState();
       }
       return () => {
           active = false;
       };
  }, [speechRecAvailable.dispatch])

  //speech commands and functions
  const zutaten = function () {
     setTimeout(function () {
       document.getElementById("back").click();
     }, 500);
   }

  const schritt1 = function () {
     setTimeout(function () {
       const element = document.getElementById("schritt1");
       element.scrollIntoView({behavior: "smooth"});
     }, 500);
  }

  const schritt2 = function () {
     setTimeout(function () {
       const element = document.getElementById("schritt2");
       element.scrollIntoView({behavior: "smooth"});
     }, 500);
  }

  const schritt3 = function () {
     setTimeout(function () {
       const element = document.getElementById("schritt3");
       element.scrollIntoView({behavior: "smooth"});
     }, 500);
  }

  const schritt4 = function () {
     setTimeout(function () {
       const element = document.getElementById("schritt4");
       element.scrollIntoView({behavior: "smooth"});
     }, 500);
  }

  const schritt5 = function () {
     setTimeout(function () {
       const element = document.getElementById("schritt5");
       element.scrollIntoView({behavior: "smooth"});
     }, 500);
  }

  const commands = {
    '(zurück zu) Zutaten': zutaten,
    '(Schritt) eins': schritt1,
    '(Schritt) zwei': schritt2,
    '(Schritt) drei': schritt3,
    '(Schritt) vier': schritt4,
    '(Schritt) fünf': schritt5,
    '(scroll) hoch': scrollUp,
    '(scroll) runter': scrollDown,
    '(scroll) ganz hoch': ganzHoch,
  };

  annyang.addCommands(commands);
  annyang.debug();

  return (
    <React.Fragment>
      <StyledContent>
        <StyledBanner>
          <img src= 'img/kohl_banner.jpg' alt= 'Bannerfoto Kohlrouladen' />
          <StyledBannerLink to="/"><img src='/img/home.svg' alt='zur Startseite'/></StyledBannerLink>
          <h1>Kohlrouladen Anleitung</h1>
        </StyledBanner>
        <StyledControlPanel>
          <StyledCallToAction>
            <Link style={{ textDecoration: 'none' }} id="back" to="/kohlrouladen"><img src="img/arrow_left.png" alt="zurück zu Zutaten" /><span>zurück zu Zutaten</span></Link>
          </StyledCallToAction>
        </StyledControlPanel>
          <StyledMainContent id='StyledMainContent'>
            <div id="schritt1">
              <h2><img src='img/arrow_right.png' alt='Verarbeitungsschritt 1' />Schritt 1</h2>
              <StyledAnleitung>
                <img src="/img/weisskohl-strunk-herausschneiden.jpg" alt="Kohlrouladen Anleitung Schritt 1" />
                <p>Kohl putzen und waschen. Struck keilförmig herausschneiden. Ganzen Kohl in reichlich
                   kochendem Salzwasser ca. 10 Minuten vorgaren, damit sich die Blätter lösen lassen.
                   Danach herausheben.
                </p>
              </StyledAnleitung>
            </div>
          <div id="schritt2">
              <h2><img src='img/arrow_right.png' alt='Verarbeitungsschritt 2' />Schritt 2</h2>
              <StyledAnleitung>
                <img src="/img/rippen-flach-schneiden-2.jpg" alt="Kohlrouladen Anleitung Schritt 2"/>
                <p>Weiche Blätter ablösen. Kohl kurz weiterkochen, nach und nach insgesamt 16 Blätter ablösen.
                   Brötchen in kaltem Wasser einweichen. Dicke Rippen zum besseren Wickeln flach schneiden.
                   Zwiebel schälen, würfeln. Mit Hack, ausgedrücktem Brötchen und Ei verkneten. Mit Salz und
                   Pfeffer würzen. 8 ovale Klöße formen.
                </p>
              </StyledAnleitung>
          </div>
          <div id="schritt3">
              <h2><img src='img/arrow_right.png' alt='Verarbeitungsschritt 3' />Schritt 3</h2>
              <StyledAnleitung>
                <img src="/img/kohlrouladen-einklappen.jpg" alt="Kohlrouladen Anleitung Schritt 3" />
                <p>Je 1 größeres und 1 kleineres Kohlblatt leicht überlappend aufeinanderlegen. Hack jeweils in die
                   Mitte legen. Die Blattseiten zur Mitte einschlagen. Kohlrouladen fest aufrollen, mit Küchengarn
                   umwickeln und fest zubinden.
                </p>
              </StyledAnleitung>
          </div>
          <div id="schritt4">
            <h2><img src='img/arrow_right.png' alt='Verarbeitungsschritt 4' />Schritt 4</h2>
            <StyledAnleitung>
              <img src="/img/rouladen-anbraten.jpg" alt="Kohlrouladen Anleitung Schritt 4"/>
              <p>Butterschmalz in einem großen Schmortopf erhitzen. Rouladen darin rundherum bei starker Hitze kräftig anbraten.
                 Brühe angießen und aufkochen. Alles zugedeckt ca. 45 Minuten schmoren.
              </p>
            </StyledAnleitung>
          </div>
          <div id="schritt5">
            <h2><img src='img/arrow_right.png' alt='Verarbeitungsschritt 5' />Schritt 5</h2>
             <StyledAnleitung>
                 <img src="/img/fond-binden.jpg" alt="Kohlrouladen Anleitung Schritt 5"/>
                 <p>Kohlrouladen aus dem Schmortopf herausnehmen und warm halten. Fond aufkochen. Mehl und 4 EL Wasser glatt rühren.
                    Fond damit binden und nochmal 5-10 Minuten köcheln lassen. Mit Salz und Pfeffer abschmecken. Alles anrichten.
                    Petersilie hacken, darüberstreuen. Zum Kohlrouladen-Rezept passen Salzkartoffeln.
                 </p>
             </StyledAnleitung>
          </div>
          </StyledMainContent>
          <SpeechHelp items={SPEECH_COMMANDS}/>
      </StyledContent>
      <VerticalNav />
    </React.Fragment>
  )
}

const StyledBannerLink = styled(Link)`
  text-decoration: none;
  color: ${styles.colors.typoBlack};

  img{
    width: 2vw;
    height: 2vw;
    margin: 4vw;
  }
`;

const StyledBanner = styled.div`
  position: relative;
  width: 100%;
  height: ${styles.space.bannerHeight};

  img{
    position: absolute;
    top: 0;
    z-index: 1;
    width: 100%;
  }

  h1{
    margin: 10vw 5vw;
    font-size: ${styles.fontSizes.xLarge};
    z-index: 6;
    position: absolute;
    top: 10%;
  }
  }
`;

const StyledContent = styled.div`
  background-color: ${styles.colors.backgroundHauptspeisen};
  height: 100%;
`;

const StyledControlPanel = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const StyledCallToAction = styled.p`
  margin: 5% auto;
  font-size: ${styles.fontSizes.medium};

  img{
    width: ${styles.fontSizes.medium}
  }

  span{
    margin-left: 1vw;
  }
`;

const StyledMainContent = styled.div`
  padding: 0 5vw 5vw 5vw;

  h2{
    font-size: ${styles.fontSizes.h2}
  }

  h2 > img{
    width: ${styles.fontSizes.large};
    margin-right: ${styles.spacings.large};
  }

  div{
    margin-bottom: ${styles.margins.paragraph};
  }
`;

const StyledAnleitung = styled.div`
  display: flex;
  flex-direction: flex-start;
  justify-content: space-between;

  p{
    margin-block-start: 0;
    margin-left: 5%;
    font-size: ${styles.fontSizes.medium};
    line-height: ${styles.lineHeight.medium};
  }
`;

export default KohlrouladenAnleitung;
