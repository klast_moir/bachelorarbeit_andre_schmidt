import React from "react";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";
import HomeButton from "./HomeButton";
import styles from '../shared/global_styles';
import {
    useSpeechStateContext,
    useSpeechStateDispatch
  } from '../shared/speechState_context';

const Home = () => {
    const dispatch = useSpeechStateDispatch();
    const speechRecAvailable = useSpeechStateContext();

    //signals that on this site no speechRec is available
    function setSpeechState(speechRecAvailable) {
        if(!speechRecAvailable){
            dispatch({type: 'SET_SPEECH_AVAILABLE', changeTo: false});
       }
     };

     React.useEffect(() => {
         let active = true;
           if(active){
                setSpeechState();
           }
           return () => {
               active = false;
           };
     }, [speechRecAvailable.dispatch])


    return(
        <React.Fragment>
            <StyledContent>
                <HomeButton />
                <StyledNavBar>
                    <StyledNavBarItem>
                        <StyledLink to="/vorspeisen"><img src='/img/starter.svg' alt='zu Übersicht Vorspeisen' /><h2>Vorspeisen</h2></StyledLink>
                    </StyledNavBarItem>
                    <StyledNavBarItem>
                        <StyledLink to="/hauptspeisen"><img src='/img/main-course.svg' alt='zu Übersicht Hauptspeisen' /><h2>Hauptspeisen</h2></StyledLink>
                    </StyledNavBarItem>
                    <StyledNavBarItem>
                        <StyledLink to="/nachspeisen"><img src='/img/dessert.svg' alt='zu Übersicht Nachspeisen' /><h2>Nachspeisen</h2></StyledLink>
                    </StyledNavBarItem>
                </StyledNavBar>
            </StyledContent>
        </React.Fragment>
    )
}

const StyledContent = styled.div`
    background-color: ${styles.colors.backgroundHome};
`;

const StyledNavBar = styled.ul`
  list-style: none;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
`;

const StyledNavBarItem = styled.li`
  padding: 10% 0 30% 0;

  h2 {
    font-size: ${styles.fontSizes.h2};
    text-align: center;
  }
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  color: ${styles.colors.typoBlack};
`;

export default Home;
