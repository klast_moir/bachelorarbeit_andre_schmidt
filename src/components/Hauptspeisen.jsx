import React from "react";
import { Link } from "react-router-dom";
import HomeButton from "./HomeButton";
import styled from "@emotion/styled";
import styles from '../shared/global_styles';
import {
    useSpeechStateDispatch
  } from '../shared/speechState_context';

//Data
const soupArray = [
  {name: 'Fischsuppe', src: '/img/Fischsuppe.jpg'},
  {name: 'Fleischsuppe', src: '/img/Kuerbiskernsuppe.jpg'},
  {name: 'Kartoffelsuppe', src: '/img/Kartoffelsuppe+Wurst.jpg'},
]

const fishArray = [
  {name: 'Knoblauchgarnelen', src: '/img/knoblauch-garnelen-mit-chili-meersalz.jpg'},
  {name: 'Lachsfilet auf Gemüse', src: '/img/lachs-braten.jpg'},
  {name: 'Skrei Filet mit Kruste', src: '/img/skrei-filet-mit-pekannusskruste.jpg'},
]

const meatArray = [
  {name: 'Rinder Tatar', src: '/img/rindertatar.jpg', link: null},
  {name: 'Wiener Schnitzel', src: '/img/wiener-schnitzel.jpg', link: null},
  {name: 'Kohlrouladen', src: '/img/kohlrouladen.jpg', link: './kohlrouladen'},
]

const Hauptspeisen = () => {
   const dispatch = useSpeechStateDispatch();

   //signals that on this site no speechRec is available
   function setSpeechState(speechRecAvailable) {
     if(!speechRecAvailable){
       dispatch({type: 'SET_SPEECH_AVAILABLE', changeTo: false});
     }
   };

   React.useEffect(() => {
     let active = true;
     if(active){
       setSpeechState();
     }
     return () => {
       active = false;
     };
   }, [dispatch])

   return (
       <React.Fragment>
         <StyledContent>
           <HomeButton />
           <StyledH1>Hauptspeisen</StyledH1>
           <StyledSection id="suppen">
             <h2>Suppen</h2>
             <StyledFoodList>
               {soupArray.map(item => <StyledFoodContainer><h3>{item.name}</h3>
                                        <StyledImage src={item.src} alt={item.name}/>
                                      </StyledFoodContainer>)}
             </StyledFoodList>
           </StyledSection>
           <StyledSection id="fish">
             <h2>Fisch</h2>
             <StyledFoodList>
               {fishArray.map(item => <StyledFoodContainer><h3>{item.name}</h3>
                                        <StyledImage src={item.src} alt={item.name}/>
                                      </StyledFoodContainer>)}
             </StyledFoodList>
           </StyledSection>
           <StyledSection id="meat">
             <h2>Fleisch</h2>
             <StyledFoodList>
               {meatArray.map(item => <StyledFoodContainer>
                                        <Link style={{ textDecoration: 'none' }} to={item.link}><h3>{item.name}</h3>
                                          <StyledImage src={item.src} alt={item.name}/>
                                        </Link>
                                      </StyledFoodContainer>)}
             </StyledFoodList>
           </StyledSection>
         </StyledContent>
       </React.Fragment>
   )
}

const StyledContent = styled.div`
  background-color: ${styles.colors.backgroundHauptspeisen};
  height: 100%;
`;

const StyledH1 = styled.h1`
  margin: 7vw 0;
  text-align: center;
  font-size: ${styles.fontSizes.banner};
`;

const StyledSection = styled.section`
  padding: 5%;

  h2{
    font-size: ${styles.fontSizes.h2};
  }
`;

const StyledFoodList = styled.div`
  display: flex;
  flex-direction: flex-start;
  justify-content: space-between;
`;

const StyledFoodContainer = styled.div`
  width: calc(100% / 3 - 5%);
  text-align: center;

  h3{
    font-size: ${styles.fontSizes.h3};
  }
`;

const StyledImage = styled.img`
  width: 100%;
`;

export default Hauptspeisen;
