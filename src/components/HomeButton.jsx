import React from "react";
import { Link } from "react-router-dom";
import styled from "@emotion/styled";
import styles from '../shared/global_styles';

const HomeButton = () => {
    return (
      <React.Fragment>
        <StyledHome>
          <StyledLink to="/"><img src='/img/home.svg' alt='zur Startseite'/></StyledLink>
        </StyledHome>
      </React.Fragment>
    )
}

const StyledHome = styled.div`
  width: 2vw;
  padding: 4vw;
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  color: ${styles.colors.typoBlack};
`;

export default HomeButton;
