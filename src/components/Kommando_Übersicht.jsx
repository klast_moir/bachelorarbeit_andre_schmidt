import React, { useState } from "react";
import styled from "@emotion/styled";
import styles from '../shared/global_styles';
import Modal from 'react-awesome-modal';

const annyang = require('annyang');

const SpeechHelp = ({items}) => {
  const [visible, setVisible] = useState(false);

  //Annyang Sprachbefehle
  const toggleHelp = function () {
    document.getElementById("Hilfebutton").click();
  };

  const commands = {
    'Hilfe (schließen)': toggleHelp,
    'Sprachsteuerung': toggleHelp,
  };

  annyang.addCommands(commands);
  annyang.debug();

  const toggleModal = () => {
    setVisible(!visible);
  }

  return (
    <StyledHelpContainer>
      <StyledButton id='Hilfebutton' onClick={() => toggleModal()}><StyledImg src='img/information_white.png' alt='Hilfe' /><span>Sprachsteuerung</span></StyledButton>
        {visible ?
          (
            <Modal
              visible={visible}
              width="80%"
              onClickAway={() => toggleModal()}
            >
              <StyledModalTop>
                <h1>How to Use?</h1>
                <p>Spracherkennung einschalten!<br />Regel: (Icon +) <span>Text = Sprachbefehl</span></p>
              </StyledModalTop>
              <StyledModalBottom>
                <h1>Übersicht Sprachbefehle <span>(Anmerkung: Teile in Klammern können weggelassen werden)</span></h1>
                <StyledFlexContainer>
                  {items.length && items.map(item => (<li key={item.index}>{item}</li>))}
                </StyledFlexContainer>
                <StyledModalButton id='Hilfebutton' onClick={() => toggleModal()}><StyledImg src='img/arrow_right.png' alt='Hilfe schließen' /><span>Hilfe schließen</span></StyledModalButton>
              </StyledModalBottom>
            </Modal>
          ) : null}
    </StyledHelpContainer>
  );
}


const StyledHelpContainer = styled.div`
  position: fixed;
  z-index: 100;
  top: 0;
  left: 0;
`;

const StyledButton = styled.button`
  border: none;
  cursor: pointer;
  background-color: transparent;
  margin-top: .5vw;

  span{
    font-size: ${styles.fontSizes.small};
    color: white;
    font-weight: bold;
  }
`;

const StyledModalButton = styled.button`
  border: none;
  cursor: pointer;
  background-color: transparent;

  span{
    font-size: ${styles.fontSizes.medium};
  }
`;

const StyledImg = styled.img`
  width: ${styles.fontSizes.medium};
  margin-right: ${styles.spacings.small};
`;

const StyledModalTop = styled.div`
  background-color: #eb3434;
  padding: ${styles.spacings.large};

  h1{
    color: white;
  }

  p{
    text-align: center;
    color: white;
    font-size: ${styles.fontSizes.medium};

    span {
      text-decoration: underline white;
    }
  }

`;

const StyledModalBottom = styled.div`
  padding: ${styles.spacings.large};

  h1 > span{
    font-size: ${styles.fontSizes.small};
  }
`;

const StyledFlexContainer = styled.ul`
  list-style: none;
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
  margin: ${styles.margins.medium};

  li{
    font-size: ${styles.fontSizes.medium};
    flex: 1 50%;
    flex-grow: 0;
  }
`;

export default SpeechHelp;
