import React from 'react';
import styled from '@emotion/styled';
import HomeButton from './HomeButton';
import { Link } from 'react-router-dom';
import styles from '../shared/global_styles';

const Vorspeisen = () => {
    return (
      <React.Fragment>
        <StyledContent>
          <HomeButton />
          <StyledP><span>Bitte wähle nur <StyledLink to="/hauptspeisen" alt='zu Übersicht Nachspeisen'>Hauptspeisen</StyledLink>!</span><br/> Dort kannst du anhand der Kohlrouladen das sprachgesteuerte Navigationskonzept ausprobieren.</StyledP>
        </StyledContent>
      </React.Fragment>
    )
}

const StyledLink = styled(Link)`
  text-decoration: none;
  font-size: ${styles.fontSizes.h2};
  color: ${styles.colors.typoBlack};
`;

const StyledContent = styled.div`
  background-color: ${styles.colors.backgroundInfo};
  height: 100%;
`;

const StyledP = styled.p`
  padding: 20% 5% 30% 5%;
  text-align: center;
  color: ${styles.colors.typoWhite};
  font-size: ${styles.fontSizes.medium};

  span {
    font-size: ${styles.fontSizes.h2};
    font-weight: bold;
  }
`

export default Vorspeisen;
