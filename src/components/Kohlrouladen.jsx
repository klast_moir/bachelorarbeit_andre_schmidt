import React from "react";
import { Link } from "react-router-dom";
import styled from "@emotion/styled";
import styles from '../shared/global_styles';
import GLOBAL_COMMANDS from '../shared/global_commands';
import SpeechHelp from './Kommando_Übersicht';
import { scrollDown, scrollUp, ganzHoch } from '../shared/global_voice_functions';
import VerticalNav from './VerticalNav';
import {
    useSpeechStateContext,
    useSpeechStateDispatch
  } from '../shared/speechState_context';

const annyang = require('annyang');
const SPECIAL_COMMANDS = ['(jetzt) kochen'];
const SPEECH_COMMANDS = SPECIAL_COMMANDS.concat(GLOBAL_COMMANDS);

const Kohlrouladen = () => {
   const dispatch = useSpeechStateDispatch();
   const speechRecAvailable = useSpeechStateContext();

   //signals that on this site no speechRec is available
   function setSpeechState(speechRecAvailable) {
     if(!speechRecAvailable){
       dispatch({type: 'SET_SPEECH_AVAILABLE', changeTo: true});
     }
   };

   React.useEffect(() => {
     let active = true;
     if(active){
         setSpeechState();
     }
     return () => {
         active = false;
     };
   }, [speechRecAvailable.dispatch])

   //Sprachbefehle - Funktion
   const kochen = function () {
     setTimeout(function () {
       document.getElementById("kochen").click();
     }, 500);
   };

   const commands = {
     '(jetzt) kochen': kochen,
     '(scroll) hoch': scrollUp,
     '(scroll) runter': scrollDown,
     '(scroll) ganz hoch': ganzHoch,
   };

   annyang.addCommands(commands);
   annyang.debug();

   return (
     <React.Fragment>
       <StyledContent>
         <StyledBanner>
           <img src= 'img/kohl_banner.jpg' alt= 'Bannerfoto Kohlrouladen' />
           <StyledBannerLink to="/"><img src='/img/home.svg' alt='zur Startseite'/></StyledBannerLink>
           <h1>Kohlrouladen</h1>
         </StyledBanner>
         <StyledMainContent>
           <h2>Zutaten</h2>
           <p>für 4 Personen</p>
           <StyledZutatenContainer>
             <ul>
               <li>1 Kopf Weißkohl (ca. 1500g)</li>
               <li>Hackfleisch gemischt (1000g)</li>
               <li>2 Eier</li>
               <li>Gemüsebrühe (1000ml</li>
               <li>2 Zwiebeln</li>
               <li>1 Brötchen</li>
               <li>Butterschmalz (4 EL)</li>
               <li>Mehl (4 EL)</li>
               <li>Petersilie (1 Bund)</li>
               <li>Salz und Pfeffer</li>
             </ul>
           </StyledZutatenContainer>
         </StyledMainContent>
         <StyledControlPanel>
             <StyledCallToAction>
                 <Link style={{ textDecoration: 'none' }} id="kochen" to="/kohlrouladen_anleitung" alt='zur Kochanleitung'><img src='img/arrow_right.png' alt='zur Kochanleitung' /><span>(jetzt) kochen</span></Link>
             </StyledCallToAction>
         </StyledControlPanel>
         <SpeechHelp items={SPEECH_COMMANDS}/>
         <VerticalNav />
       </StyledContent>
     </React.Fragment>
   )
 }

const StyledBannerLink = styled(Link)`
  text-decoration: none;
  color: ${styles.colors.typoBlack};

  img{
    width: 2vw;
    height: 2vw;
    margin: 4vw;
  }
`;

const StyledBanner = styled.div`
  position: relative;
  width: 100%;
  height: ${styles.space.bannerHeight};

  img{
    position: absolute;
    top: 0;
    z-index: 1;
    width: 100%;
  }

  h1{
    margin: 10vw 40vw;
    font-size: ${styles.fontSizes.xLarge};
    z-index: 6;
    position: absolute;
    top: 10%;
  }
`;

const StyledContent = styled.div`
  background-color: ${styles.colors.backgroundHauptspeisen};
  height: 100%;

  h2{
    margin: 5% 0;
    text-align: center;
    font-size: ${styles.fontSizes.h2};
  }
`;

const StyledControlPanel = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const StyledMainContent = styled.section`
  display: flex;
  flex-direction: column;
  text-align: center;
  font-size: ${styles.fontSizes.medium};
`;

const StyledZutatenContainer = styled.div`
  font-size: ${styles.fontSizes.medium};

  ul {
    display: inline-block;

    li{
      text-align: start;
    }
  }
`;

const StyledCallToAction = styled.p`
  width: 20vw;
  margin: 5% auto;
  font-size: ${styles.fontSizes.medium};

  img{
    width: ${styles.fontSizes.large};
    margin-right: ${styles.spacings.medium};
  }

  span {
    font-size: ${styles.fontSizes.h3};
    font-weight: bold;
  }
`;

export default Kohlrouladen;
